#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import logging
from datetime import datetime
from google.appengine.ext import ndb

class User (ndb.Model):
   first_name = ndb.StringProperty()
   last_name = ndb.StringProperty()
   email = ndb.StringProperty()
   signup_token = ndb.StringProperty()
   expiration_date = ndb.DateTimeProperty()

class Club (ndb.Model):
   club_name = ndb.StringProperty()

class Event(ndb.Model):
   event_name= ndb.StringProperty()
   location= ndb.StringProperty()
   event_time_date= ndb.DateTimeProperty()
   description= ndb.StringProperty()
   club_key = ndb.KeyProperty(kind=Club)

class Alias(ndb.Model):
    alias = ndb.StringProperty()
    user = ndb.KeyProperty()


class MainHandler(webapp2.RequestHandler):
   def get(self):
       self.response.out.write('<html><body>')
       self.response.out.write(""" <h1>Enter User</h1>
           <form action="/confirm" method="post">
           first_name: <input type="text" name = "first_name">
           <br>
           last_name: <input type= "text" name = "last_name">
           <br>
           email:<input type="text" name = "email">
           <br>
           club_name: <input type="text" name = "club_name">
           <br>
           <input type= "submit" value="submit"/>
           <br>
           </form>
           <table>""")

       users = User.query().fetch()

       row_template = '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>'
       for user in users:
           #logging.info(user)
         self.response.write(row_template % (user.first_name, user.last_name, user.email, 'FIX'))
       self.response.out.write('</table></body></html>')

       self.response.out.write('<html><body>')
       self.response.out.write("""<h1>Enter Events</h1>
           <form action="/save" method="post">
           name_event: <input type= "text" name = "name_event">
           <br>
           location: <input type= "text" name = "location">
           <br>
           event_time_date: <input type= "text" name = "event_time_date">
           <br>
           description: <input type = "text" name = "description">
           <br>
           club_name: <select name = "club_name">""")
       
       clubs = Club.query().fetch()
       for club in clubs:
            self.response.out.write('<option value="' + club.key.urlsafe() + '">' + club.club_name + '</option>')
       self.response.out.write("""</select>
           <br>
           <input type= "submit" value="submit"/>
           </form>
           """)
       
       self.response.out.write('</body></html>')



class SubmitForm(webapp2.RequestHandler):
   def post(self):
       first_name = self.request.get('first_name')
       last_name = self.request.get('last_name')
       email = self.request.get('email')
       club_name = self.request.get('club_name')

       user=User()
       user.first_name = first_name
       user.last_name = last_name
       user.email = email
       user.put()

       club = Club()
       club.club_name = club_name
       club.put()
       
       self.redirect('/')

class EventForm(webapp2.RequestHandler):
   def post(self):
        name_event = self.request.get('name_event')
        location = self.request.get('location')
        event_time_date= self.request.get('event_time_date')
        description= self.request.get('description') 
        club_key_urlsafe = self.request.get('club_name')
        club_key = ndb.Key(urlsafe=club_key_urlsafe)

        event=Event()
        event.name_event= name_event
        event.location= location
        event.event_time_date= datetime.strptime(event_time_date, '%m/%d/%Y')
        event.description= description
        event.club_key = club_key
        event.put()
        self.redirect('/')

    #def get_parent_key(user):
    #return ndb.Key("club_name", user.club_name())
    #def print_query():
    #print(user.query())

class PostUsers(webapp2.RequestHandler):
    def post(self):
        first_name = 'NOT_SET' #self.request.get('first_name')
        last_name = 'NOT_SET' #self.request.get('last_name')
        email = self.request.get('email')
        
        if email.endswith("mville.edu"):
            user=User()
            user.first_name = first_name
            user.last_name  = last_name
            user.email      = email
            user.signup_token = generate_code()
            now = datetime.datetime.now()
            future = now + datetime.timedelta(minutes=15)
            user.expiration_date = future
            user_key = user.put()
            key_id = long(user_key.id())
            alias = aliases[key_id % len(aliases)]
            alias_entity = Alias(alias=alias, user=user_key)
            alias_entity.put()
            email_text = """
                Welcome to CampusLife! Your access code is %s. Your alias is "%s". You can request a new alias at any time by visiting the CampusLife web site, http://mvilleclub.appspot.com.
                
                Luiza Vukaj,Addia Khursheed CampusLife creators
                """ % (user.signup_token, alias)
            mail.send_mail(sender="CampusLife Admin <luvukaj@gmail.com>",
                           to=email,
                           subject="Welcome, %s! Here is your CampusLife access code" % (alias),
                           body=email_text)
                           
            return_obj = {'success': True, 'message': alias}
        else:
            return_obj = {'success': False, 'message': 'NOTMVILLE'}
        
        #        return_obj = {'code':user.signup_token}
        self.response.content_type = 'application/json'
        self.response.write(json.dumps(return_obj))

def generate_code():
    while True:
        code = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
        if User.query(User.signup_token==code).count()==0:
            break
    return code

# /validate
class ValidateSignUpCodeHandler(webapp2.RequestHandler):
    # Given a sign-up code and an email address, validates that it is a valid code
    # for that email address and has not expired. Returns a token that can be used for
    # authentication
    def post(self):
        email = self.request.get('email')
        code = self.request.get('code')
        user = User.query(User.email==email, User.signup_token==code).get()
        self.response.content_type = 'application/json'
        if user:
            expiration = user.expiration_date
            now = datetime.datetime.now()
            if now < expiration:
                return_obj = {'success':True, 'token': user.key.urlsafe()}
                self.response.write(json.dumps(return_obj))
            else:
                return_obj = {'success':False, 'message': 'EXPIRED'}
                self.response.write(json.dumps(return_obj))
        else:
            return_obj = {'success':False, 'message': 'INVALID'}
            self.response.write(json.dumps(return_obj))





app = webapp2.WSGIApplication([
                              ('/', MainHandler),
                              ('/confirm', SubmitForm),
                              ('/save',EventForm)
                              ], debug=True)
