package com.example.mvillecampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText edEmail = (EditText) findViewById(R.id.edEmail);
        final EditText edPassword = (EditText) findViewById(R.id.edPassword);
        final TextView registerHere = (TextView) findViewById(R.id.registerHere);
        final Button SignInButton = (Button) findViewById(R.id.SignInButton);

        registerHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);


            }
        });


        SignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent HomeScreen = new Intent(LoginActivity.this, HomeScreen.class);
                LoginActivity.this.startActivity(HomeScreen);


            }
        });



    }
}
