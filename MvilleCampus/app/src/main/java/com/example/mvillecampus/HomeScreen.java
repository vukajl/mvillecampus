package com.example.mvillecampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class HomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        final ImageButton eventButton = (ImageButton) findViewById(R.id.eventButton);
        final ImageButton clubButton = (ImageButton) findViewById(R.id.clubButton);
        final ImageButton settingsButton = (ImageButton) findViewById(R.id.settingsButton);
        final ImageButton adminSignin = (ImageButton) findViewById(R.id.adminSignin);
        final ImageButton favoritesButton = (ImageButton) findViewById(R.id.favoritesButton);
        final ImageButton organizationButton = (ImageButton) findViewById(R.id.organizationButton);


        organizationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Organization = new Intent(HomeScreen.this, HomeScreen.class);
                HomeScreen.this.startActivity(Organization);


            }
        });


        favoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Favorites = new Intent(HomeScreen.this, HomeScreen.class);
                HomeScreen.this.startActivity(Favorites);


            }
        });


        adminSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SignIn = new Intent(HomeScreen.this, HomeScreen.class);
                HomeScreen.this.startActivity(SignIn);


            }
        });


        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Setting = new Intent(HomeScreen.this, HomeScreen.class);
                HomeScreen.this.startActivity(Setting);


            }
        });




        clubButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Club = new Intent(HomeScreen.this, HomeScreen.class);
                HomeScreen.this.startActivity(Club);


            }
        });


        eventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Event = new Intent(HomeScreen.this, HomeScreen.class);
                HomeScreen.this.startActivity(Event);


            }
        });

    }
}
